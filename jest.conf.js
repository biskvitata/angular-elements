module.exports = {
  preset: 'jest-preset-angular',
  setupTestFrameworkScriptFile: './src/jest.ts',
  moduleFileExtensions: ['js', 'json', 'ts'],
  testPathIgnorePatterns: ['/node_modules/'],
  testResultsProcessor: 'jest-bamboo-reporter',
  testURL: 'http://localhost',
  coverageDirectory: './coverage',
  collectCoverageFrom: [
    'src/app/**/*.ts',
    '!src/app/ui-demo/**/*.ts',
    '!**/*.abstract.ts',
    '!**/*.const.ts',
    '!**/*.d.ts',
    '!**/*.enum.ts',
    '!**/*.mock.ts',
    '!**/*.model.ts',
    '!**/*.module.ts',
    '!**/*.setup.ts'
  ],
  coverageReporters: ['html', 'lcov', 'json', 'text'],
  coverageThreshold: {
    global: {
      branches: 0,
      functions: 0,
      lines: 0,
      statements: 0
    }
  }
};