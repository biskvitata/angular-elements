/**
 * Note & How To Use
 *
 * THIS IS DEVELOPMENT ONLY FILE
 * ...for purpose of defining all settings used in developing and testing.
 *
 * During the build (`ng build ---prod`) this file will be replaced with `environment.prod.ts`.
 * This will ensure executing of unit test with no complains on undefined enviromant variables.
 *
 * FOR PRODUCTION READY... visit `environment.prod.ts` file.
 * Handling settings related to the DEPLOYMENT ENVIRONMENTS in production should be handled there.
 *
 * More detailed explanation on https://confluence.neterra.paysafe.com/pages/viewpage.action?pageId=164464273&src=contextnavpagetreemode
 */

export const environment = window['PS_SETTINGS'] || {
  /* Settings for development purpose */
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
