import 'babel-polyfill';
import 'jest-preset-angular';

Object.defineProperty(window, 'getComputedStyle', {
  value: () => ['-webkit-appearance']
});

Object.defineProperty(document.body.style, 'transform', {
  value: () => {
    return {
      enumerable: true,
      configurable: true
    };
  }
});

// Mocking localStorage and sessionStorage.
const mock = () => {
  let storage = {};
  return {
    getItem: (key) => (key in storage ? storage[key] : null),
    setItem: (key, value) => (storage[key] = value || ''),
    removeItem: key => delete storage[key],
    clear: () => (storage = {})
  };
};

Object.defineProperty(window, 'localStorage', { value: mock() });
Object.defineProperty(window, 'sessionStorage', { value: mock() });

// polyfill/shim for supporting innerText
Object.defineProperty(Element.prototype, 'innerText', {
  get: () => {
    return this.innerHTML.replace(/<[^>]+>/g, '').trim();
  },
  set: (val) => {
    this.innerHTML = val;
  }
});

Element.prototype.closest = function(s) {
  let el = this;
  if (!document.documentElement.contains(el)) {
    return null;
  }
  do {
    if (el.matches(s)) {
      return el;
    }
    el = el.parentElement || el.parentNode;
  } while (el !== null && el.nodeType === 1);
  return null;
};
