'use strict';

/**
 * Install the following dependencies in order to use proxy features:
 * - chrome-launcher: ^0.10.5,
 * - paysafe-gpf-as-http-proxy-middleware-body-replace: ^0.1.1
 *
 * EXAMPLE ON:
 * 1) Serving the app inside Chrome incognito mode as workaround for CORS
 * 2) Proxing requests in development environment
 */

const chromeLauncher = require('chrome-launcher');
const createProxyBodyReplaceHandler = require('paysafe-gpf-as-http-proxy-middleware-body-replace');


/**
 * Serving the app inside Chrome incognito mode as workaround for CORS
 */

// chromeLauncher.launch({
//   startingUrl: 'http://localhost:4200/ae',
//   chromeFlags: [
//     '--incognito',
//     '--start-maximized',
//     '--disable-popup-blocking',
//     '--disable-web-security' // to enable CORS
//   ]
// }).then(chrome => {
//   console.log(`Chrome debugging port running on ${chrome.port}`);
// });



/**
 * Proxing requests in development environment
 */

 const proxyConfig = [
  {
    // Proxy everything that starts with `/api`
    context: ['/api'],
    // Targeting proxied to some another URL.
    target: 'https://transfers-dev.skrill.com',
    secure: false,
    changeOrigin: true,
    // origin value have to be the same as target for proxy.
    headers: {
      'Access-Control-Allow-Origin': "*",
      origin: 'https://transfers-dev.skrill.com'
    },
    logLevel: 'debug',
  //   // Rewriting a URL fragment
  //   pathRewrite: {
  //     "<url-fragment>": "<replace-with>"
  //   },
  //   // Modify certain part of proxied response
  //   onProxyRes: createProxyBodyReplaceHandler(
  //     (url, contentType, statusCode) => {
  //       let urlWithoutParams = url.split('?')[0];
  //       return urlWithoutParams == '<certain-url-fragment>';
  //     },
  //     (url, contentType, statusCode, body) => {
  //       return body.replace(
  //         `<search-for>`,
  //         `<replace-with>`
  //       );
  //     }
  //   ),
  }
];

module.exports = proxyConfig;
