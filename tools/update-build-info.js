const data = require('../package.json');
const { name, version } = data;
const { projects } = require('../angular.json');

"use strict";

// Commit id value
const commit = new Promise(function(resolve, reject) {
  require("child_process").exec("git log -1", function(err, stdOut) {
    // console.log('commit >', stdOut);
    resolve(stdOut.substr(7, 40));
  });
});

// Branch name value
const branch = new Promise(function(resolve, reject) {
  require("child_process").exec("git rev-parse --abbrev-ref HEAD", function(
    err,
    stdOut
  ) {
    // console.log('branch >', stdOut);
    resolve(stdOut.replace(/\r?\n|\r/, ""));
  });
});

// Update info.json file
Promise.all([commit, branch]).then(function(r){
  const infoFilename = `../${projects[name].architect.build.options.outputPath}/deployment/metadata/info.json`;
  var info = require(infoFilename);

  info.app.version = version;
  info.app.gitCurrentBranch = r[1];
  info.app.gitChangeSet = r[0];
  info.app.buildDate = new Date();

  // console.log('info', info);

  require("fs").writeFile(
    `${__dirname}/${infoFilename}`,
    JSON.stringify(info, null, 2),
    function(err) {
      if (err) {
        console.log(`ERROR while writing 'info.json' file. Details:\n`, err);
      } else {
        console.log(`Finished updating of 'info.json' file.\n`);
      }
    }
  );
})
