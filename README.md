# example-angular-elements-fe

Angular Elements example application


# Changelog

## vx.y.z
  - Feature: ...
  - ...
  - Fix: ...
  - ...


# Development

This frontend project was generated with [Peon](https://peon.dev.skrillws.net/) project generator.
It is based on [Angular CLI](https://github.com/angular/angular-cli) and all the CLI commands are inherited and valid to use.

Peon-like project have modifications and additions which will ensure faster and comfortable development aligned with general [Frontend Pipeline](https://confluence.neterra.paysafe.com/display/GPF/WIP%3A+Frontend+Pipeline?src=contextnavpagetreemode) defined through project structure, development, testing, deployment, and monitoring.

The project includes `app-routing.module.ts` and styles are empowered by SCSS.

The `/.npmrc` file ensures proper authorization for local [Artifactory](https://artifactory.neterra.paysafe.com).

The `/sonar-project.properties` contains sonar configuration for the project.

Added `/src/deployment` folder should ensure running the app on different environments on-the-fly. 

Set `module` to `commonjs` value in `/src/tsconfig.spec.json` file, required by Jest. Files `/jest.conf.js` and `/src/jest.ts` are also used to setting Jest runner properly.


## Serve

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files. `npm run start` does the same.

`npm run start:proxy` serve allow us to serve the app with some proxy configured in `/tools/proxy.conf.js`. Under the hood it is [http-proxy-middleware](https://github.com/chimurai/http-proxy-middleware) - use the README to set the configuration file properly. This proxy feature allow us to use remote backend end points for requests during the development or even to modify some responses with `paysafe-gpf-as-http-proxy-middleware-body-replace` [library](https://bitbucket.neterra.paysafe.com/projects/GPF/repos/paysafe-gpf-as-http-proxy-middleware-body-replace/browse).


## Build

Runing `npm run build` will execute `ng build --prod` command preceded by linting (`ng lint`) and unit testing (`npm run test:quick`). This results with:
* The build artifacts will be stored in the `/dist/<base-href>` directory. 
* `/src/deployment` folder will be part of build artifacts as well.
* SourceMap files are generated.

Thereafter, `postbuild` script will be started automatically. This will fill in `/dist/deployment/metadata/info.json` file with build-info data `version`, `gitCurrentBranch`, `gitChangeSet`, and `buildDate`.

### Linting

Default AngularCLI linting is extended. New rules are added into `tslint.json` file.

Additionally, `tslint-sonarts` is implemented in order to align the code in the same manner as Sonar.

By default, linting will be started as conditioned task by `npm run build` command.

## Deployment environment settings

For local development settings use `/src/environments/environment.ts`. All setting goes into the fallback object.

**Do not use the same principle for production!**

Do not modify `/src/environments/environment.prod.ts` file. Do not use Angular `/src/environments` capabilities for managing environment settings for different deployment (like `fra-prod`, `sfa-dev`, etc.).

All deployment environment setting goes in `/src/deployment/env` files: 

```bash
/src/deployment/env
  fra-prod.fragment.html
  sfa-dev.fragment.html
  sfa-qa.fragment.html
  vie-prod.fragment.html
```

These `*.fragment.html` files can contain any HTML-valid content, including JavaScript and will be included automatically by deployment server and NGINX in `<head>` block of [index.html](./src/index.html) file.

If you need additional deployment to cover - just add new file with appropriate name `<deployment-name>.fragment.html`.

File `/src/environments/environment.prod.ts` targets these included fragment with `window['PS_SETTINGS']`.

### Need additional environment configuration?

Any additional environment configuration (like implementing Hot Module Replacement) should be handled like `environment.ts`.

## Packaging

The Bamboo build plan will create a ZIP archive of `/dist/<base-href>` folder with `<project-name>-<version>.zip` as the filename.


## Running unit tests

[Jest](https://jestjs.io) is installed as default test runner in order to ensure much faster and reliable executing. Related command are `npm run test`, `npm run test:watch`, and `npm run test:quick`. Execution is limited with `--maxWorkers=2` parameter [to avoid performance throttling](https://jestjs.io/docs/en/cli.html#maxworkers-num) on CI.

By default, `npm run test:quick` will be started as conditioned task by `npm run build` command.

[Karma](https://karma-runner.github.io) is not removed and it is still available to use by running `npm run ngtest`.


## Running end-to-end tests

Executing the end-to-end tests is via [Protractor](http://www.protractortest.org/).

Mocking the requests and responses are under development and will be available soon.


## Further help

To get more help on general frontend topics check out the [Frontend Pipeline page](https://confluence.neterra.paysafe.com/display/GPF/WIP%3A+Frontend+Pipeline?src=contextnavpagetreemode) or contact [Atom team](mailto:atom-team@paysafe.com).

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
